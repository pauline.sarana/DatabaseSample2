package com.example.pauline.databasesample2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent




class MainActivity : AppCompatActivity() {
    var myDB: DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        myDB = DatabaseHelper(this);

        add.setOnClickListener {
            val newEntry = editText.getText().toString()
            if (editText.length() !== 0) {
                AddData(newEntry)
                editText.setText("")
            } else {
                Toast.makeText(this@MainActivity, "You must put something in the text field!", Toast.LENGTH_LONG).show()
            }
        }

        view.setOnClickListener{
            val intent = Intent(this@MainActivity, ViewListContents::class.java)
            startActivity(intent)
        }
    }

    fun AddData(newEntry: String) {

        val insertData = myDB!!.addData(newEntry)

        if (insertData == true) {
            Toast.makeText(this, "Data Successfully Inserted!", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Something went wrong :(.", Toast.LENGTH_LONG).show()
        }
    }
}
