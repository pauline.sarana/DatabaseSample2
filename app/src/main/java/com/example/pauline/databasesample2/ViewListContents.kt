package com.example.pauline.databasesample2

import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import android.os.Bundle
import kotlinx.android.synthetic.main.viewlistcontents_layout.*
import java.util.Arrays.asList




/**
 * Created by Pauline on 11/21/2017.
 */
class ViewListContents : AppCompatActivity() {
    var myDB: DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.viewlistcontents_layout)

        myDB = DatabaseHelper(this)

        //populate an ArrayList<String> from the database and then view it
        val theList : ArrayList<String>? = ArrayList()
        val data : Cursor = myDB!!.getListContents()
        if (data.count == 0) {
            Toast.makeText(this, "There are no contents in this list!", Toast.LENGTH_LONG).show()
        } else {
            while (data.moveToNext()) {
                if (theList != null) {
                    theList.add(data.getString(1))
                }
                val listAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, theList)
                listView.adapter = listAdapter
            }
        }


    }
}